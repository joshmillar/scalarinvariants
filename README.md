OpenFOAM function object to compute the five scalar invariants explained in 
Leschziner (2016). 

To install, source openfoam environment and run
``
wmake libso
``
Then add the library to your control dict and run:
``
postProcess -func scalarInvariants -fields "(U k epsilon)"
``
This will create a symmTensorField, which has just been used for convenience, 
there is no dimensional info embedded. The first value is zero then lambda1 
though lambda 5. 